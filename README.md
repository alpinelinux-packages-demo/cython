# [cython](https://pkgs.alpinelinux.org/packages?name=cython&arch=x86_64)

The Cython compiler for writing C extensions for the Python language https://cython.org/

* https://gitlab.com/python-packages-demo/cython
* https://gitlab.com/debian-packages-demo/cython

* https://en.wikipedia.org/wiki/Cython